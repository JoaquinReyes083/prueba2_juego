//Tecla Espacio
document.addEventListener("keydown", function (evento) {
  if (evento.keyCode == 32) {
    if (nivel.muerto == false) {
      saltar();
    }
    else {
      nivel.velocidad = 9;
      nivel.muerto = false;
      nube.velocidad = 1;
      obs.x = ancho + 100;
      nube.x = ancho + 100;
      nivel.marcador = 0;
    }
  }
})
var canvas;
var ctx;
//Click
window.onload = function () {
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  canvas.addEventListener("click", click(), false);
}
function click() {
  if (nivel.muerto == false) {
    saltar();
  }
  else {
    nivel.velocidad = 9;
    nivel.muerto = false;
    nube.velocidad = 1;
    obs.x = ancho + 100;
    nube.x = ancho + 100;
    nivel.marcador = 0;
  }
}
//-------------------------------------------------------------------------------
var imgPikachu;
var imgObstaculo;
var imgNube;
var imgSuelo;

function cargaImagenes() {
  imgPikachu = new Image();
  imgObstaculo = new Image();
  imgNube = new Image();
  imgSuelo = new Image();

  imgPikachu.src = "img/pikachu.gif";
  imgObstaculo.src = "img/obstaculo.png";
  imgNube.src = "img/nube.png";
  imgSuelo.src = "img/suelo.png"
}

var ancho = 700;
var alto = 300;
//-------------------------------------------------------------------------------
function inicializa() {
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  cargaImagenes();
}
//-------------------------------------------------------------------------------
function borraCanvas() {
  canvas.width = ancho;
  canvas.height = alto;
}
//-------------------------------------------------------------------------------
var suelo = 200;
var pika = { y: suelo, vy: 0, gravedad: 2, salto: 28, vymax: 9, saltando: false };
var nivel = { velocidad: 9, marcador: 0, muerto: false };
var obs = { x: ancho + 100, y: suelo };
var nube = { x: 400, y: 100, velocidad: 1 };
var SueloM = { x: 0, y: suelo + 30 };
//-------------------------------------------------------------------------------
function dibujaPikachu() {
  ctx.drawImage(imgPikachu, 0, 0, 400, 400, 100, pika.y, 60, 60);
}
//-------------------------------------------------------------------------------
function dibujaObs() {
  ctx.drawImage(imgObstaculo, 0, 0, 512, 512, obs.x, obs.y, 50, 50);
}

function logicaObs() {
  if (obs.x < -100) {
    obs.x = ancho + 100;
    nivel.marcador++;
  }
  else {
    obs.x -= nivel.velocidad;
  }
}
//-------------------------------------------------------------------------------
function logicaNube() {
  if (nube.x < -100) {
    nube.x = ancho + 100;
  }
  else {
    nube.x -= nube.velocidad;
  }
}

function dibujaNube() {
  ctx.drawImage(imgNube, 0, 0, 512, 512, nube.x, nube.y, 60, 50);
}
//-------------------------------------------------------------------------------
function dibujaSuelo() {
  ctx.drawImage(imgSuelo, SueloM.x, 0, 700, 517, 0, SueloM.y, 700, 150);
}

function logicaSuelo() {
  if (SueloM.x > 700) {
    SueloM.x = 0;
  }
  else {
    SueloM.x += nivel.velocidad;
  }
}
//-------------------------------------------------------------------------------
function saltar() {
  if (pika.saltando == false) {
    pika.saltando = true;
    pika.vy = pika.salto;
  }

}
//-------------------------------------------------------------------------------
function gravedad() {
  if (pika.saltando == true) {
    if (pika.y - pika.vy - pika.gravedad > suelo) {
      pika.saltando = false;
      pika.vy = 0;
      pika.y = suelo;
    } else {
      pika.vy -= pika.gravedad;
      pika.y -= pika.vy;
    }
  }
}

function colision() {
  if (obs.x >= 100 && obs.x <= 150) {
    if (pika.y >= suelo) {
      nivel.muerto = true;
      nivel.velocidad = 0;
      nube.velocidad = 0;
    }
  }
}

function puntuacion() {
  ctx.font = "30px impact";
  ctx.fillStyle = "#555555";
  ctx.fillText(`${nivel.marcador}`, 600, 50);

  if (nivel.muerto == true) {
    ctx.font = "60px impact";
    ctx.fillText(`GAME OVER`, 240, 150);
  }
}

//Bucle Principal

var FPS = 50;
setInterval(function () {
  principal();
}, 1000 / FPS);

function principal() {
  borraCanvas();
  gravedad();
  colision();
  logicaObs();
  logicaNube();
  dibujaSuelo();
  dibujaObs();
  dibujaNube();
  dibujaPikachu();
  puntuacion();
}